from flask import Flask, render_template, request, redirect, url_for, abort, session
import feedparser
from getFeeds import search_solr


app = Flask(__name__)
app.config['SECRET_KEY'] = 'F34TF$($e34D';

@app.route("/")
def hello():
    news,word_cloud = search_solr('*')
    return render_template("index.html",words=word_cloud)

@app.route("/google")
def ndtv_feed():
    d = feedparser.parse("http://news.google.com/?output=rss")
    return render_template("channelTable.html", data=d, name='Google')

@app.route("/mashable")
def mashable_feed():
    d = feedparser.parse("http://feeds.mashable.com/Mashable")
    return render_template("channelTable.html", data=d, name='Mashable')

@app.route("/search",methods=['POST'])
def search():
    session['search_query'] = request.form['search_query']
    return redirect(url_for('result'))

@app.route("/result")
def result():
    print "kiran"
    query = session['search_query']
    print query
    news,word_cloud = search_solr(query)
    return render_template('dataTable.html', data=news)


@app.route("/searching/<name>",methods=['GET'])
def searching(name):
    news,word_cloud = search_solr(name)
    return render_template('dataTable.html', data=news)

if __name__ == "__main__":
    app.run(debug=True)
