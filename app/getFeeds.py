import feedparser, threading
from mysolr import Solr
from bs4 import BeautifulSoup

# create a connection to a solr server
s = Solr()

#list of rss urls to be parsed
hit_list = ['http://www.reddit.com/r/technology/.rss',
            'https://news.ycombinator.com/rss',
            'http://www.reddit.com/r/programming/.rss',
            'https://news.google.co.in/news/feeds?pz=1&cf=all&ned=in&hl=en&topic=tc&output=rss',
            'https://news.google.co.in/news/feeds?pz=1&cf=all&ned=in&hl=en&topic=snc&output=rss',
            'http://feeds2.feedburner.com/dottechdotorg',
            'http://feeds.mashable.com/Mashable'       
        ]

get = []

#appending all the parsing feeds 
for lists in hit_list:
        get.append(feedparser.parse(lists))

        
#extracting feeds from reddit.com
def extract_feeds_and_index(): 
        #indexing the link/id and title into solr from feeds
        for feed in get:
                for item in feed["items"]:
                        try:
                                soup = BeautifulSoup(item.description)
                                img_url = soup.find("img")["src"]
                        except:
                                img_url = ""
                        documents = [
                                {"id":item.title,"title":item.title,"links":item.link,"url":img_url}
                        ]
                        s.update(documents, 'json', commit=True)
                        s.commit()
        threading.Timer(1000,extract_feeds_and_index).start()

#query
def search_solr(query_item):
        #get docs and facets
        query = {'q' : query_item, 'rows':'2000', 'start':'0', 'facet' : 'true', 'facet.field' : 'title', 'facet.limit' : 30, 'facet.sort' : 'true'}
        response = s.search(**query)
        documents = response.documents
        facets = response.facets

        result_word_cloud = dict()
        #result from facets for getting the no. of hits on topic
        result_word_cloud ={ key:facets['facet_fields']['title'][key] for key in facets['facet_fields']['title'] }

        
        #result from docs on search query
        #for result_news in documents:
        #        try:
        #                print result_news['url']
        #        except:
        #                result_news['url'] = ""
        return documents, result_word_cloud

