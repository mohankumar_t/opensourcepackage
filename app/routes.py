from flask import Flask, render_template
from getFeeds import search_solr

app = Flask(__name__)     
 
#home page
@app.route('/')
def home():
  news,word_cloud = search_solr('microsoft')
  return render_template('home.html',news_array=news,word_array=word_cloud)
 
if __name__ == '__main__':
  app.run(debug=True)
