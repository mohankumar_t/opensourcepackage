import feedparser, threading
from mysolr import Solr
from bs4 import BeautifulSoup

# create a connection to a solr server
s = Solr()

#list of rss urls to be parsed
hit_list = ['http://www.reddit.com/r/technology/.rss',
            'https://news.ycombinator.com/rss',
            'http://www.reddit.com/r/programming/.rss',
            'https://news.google.co.in/news/feeds?pz=1&cf=all&ned=in&hl=en&topic=tc&output=rss',
            'https://news.google.co.in/news/feeds?pz=1&cf=all&ned=in&hl=en&topic=snc&output=rss',
            'http://feeds2.feedburner.com/dottechdotorg',
             'http://feeds.mashable.com/Mashable'       
        ]
get = []

#appending all the parsing feeds 
for lists in hit_list:
        get.append(feedparser.parse(lists))
        
#extracting feeds from reddit.com
def extract_feeds_and_index(): 
        #indexing the link/id and title into solr from feeds
        for feed in get:
                for item in feed["items"]:
                        #print item['date']
                        try:
                                soup = BeautifulSoup(item.description)
                                img_url = soup.find("img")["src"]
                        except:
                                img_url = ""
                        documents = [
                                {"id":item.title,"title":item.title,"links":item.link,"url":img_url}
                        ]
                        s.update(documents, 'json', commit=True)
                        s.commit()
        threading.Timer(1000,extract_feeds_and_index).start()

def search(item):
        query = {'q' : item, 'rows':'10', 'start':'0', 'facet' : 'true', 'facet.field' : 'title'}
        response = s.search(**query)
        # Get documents
        documents = response.documents
        #print documents
        # Get facets
        facets = response.facets
        #print facets['facet_fields']['title']
        for result in documents:
                print result['title'] + result['links']
                result['image'] = get_img_url(result)
                #print result['image']
        return result

def get_img_url(item):
        img_url = ""
        for feed in get:
                for item in feed["items"]:
                        try:
                                soup = BeautifulSoup(item.description)
                                img_url = soup.find("img")["src"]
                        except:
                                img_url = ""
        return img_url

#extract_feeds_and_index()
search('*')
